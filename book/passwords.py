#! c:\python34\python.exe
#!/usr/bin/env python
##demo code provided by Steve Cope at www.steves-internet-guide.com
##email steve@steves-internet-guide.com
##Free to use for any purpose

broker="test.mosquitto.org"
broker="192.168.1.41"
#broker="localhost"

"""
Passwords demo code- Shows how to set user name and password.
Sending a username/password when none is required is ok the broker
ignores them. Logging is enabled so you can see more detail
"""

import paho.mqtt.client as mqtt  #import the client
import time

QOS1=1
QOS2=1
CLEAN_SESSION=True


def on_disconnect(client, userdata, flags, rc=0):
    m="DisConnected flags"+"result code "+str(rc)
    print(m)
def on_log(client, userdata, level, buf):
        print("log: "+buf)
def on_connect(client, userdata, flags, rc):
    print("Connected flags ",str(flags),"result code ",str(rc))

def on_message(client, userdata, message):
    print("message received  "  ,str(message.payload.decode("utf-8")))
def on_publish(client, userdata, mid):
    print("message published "  ,str(message.payload.decode("utf-8")))

print("creating client 1 with clean session set to",CLEAN_SESSION)
client = mqtt.Client("Python1",clean_session=CLEAN_SESSION)    #create new instance
## edit code for passwords
print("setting  password")
client.username_pw_set(username="roger",password="password")
##
client.on_message=on_message        #attach function to callback
client.on_connect=on_connect
client.on_log=on_log
print("connecting to ",broker)
client.connect(broker)
client.loop_start()
#client.on_disconnect=on_disconnect
time.sleep(3)
#client.loop()
client.disconnect()
client.loop_stop()
##



