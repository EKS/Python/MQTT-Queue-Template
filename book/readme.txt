Thank you for purchasing my book on Python and MQTT. The scripts here are designed to reinforce the code snippets in the book and give yo a starting point for you own code.
I would also kindly ask you to take time and review the book on Amazon as that will help me greatly when I update the book to cover MQTTv5.
If you have any questions or comments then please use the ask steve page on the site 
http://www.steves-internet-guide.com/ask-steve/

and don't forget to sign up for the newsletter.
http://www.steves-internet-guide.com/newsletter/

Good luck with Python and MQTT
