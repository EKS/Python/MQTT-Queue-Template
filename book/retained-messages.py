#! c:\python34\python.exe
#!/usr/bin/env python
##demo code provided by Steve Cope at www.steves-internet-guide.com
##email steve@steves-internet-guide.com
##Free to use for any purpose
"""
This script will check for retained messages and then publish a message with retained
flag set true,unsubscribe and the subscribe again to view the retained message.
Logging is enable to see more detail. At the end we clear the retained
messages from the broker by pubishing an empty message with the retained
flag set to True.


"""
import paho.mqtt.client as mqtt  #import the client
import time

broker="test.mosquitto.org"
broker="192.168.1.41"
#broker="localhost"
port=1883
QOS0=0
QOS1=0
QOS2=0
RETAIN=True
CLEAN_SESSION=True
cname="retained-script"
USERNAME=""
PASSWORD=""
port=1883

mqttclient_log=False #client logging
verbose=True
topic="house/#"
#List and optionally delete retained messages

retained_topics=[]
def on_log(client, userdata, level, buf):
    print("log: ",buf)
def Initialise_client_object():  
    mqtt.Client.bad_connection_flag=False
    mqtt.Client.suback_flag=False
    mqtt.Client.connected_flag=False
    mqtt.Client.disconnect_flag=False


def Initialise_clients(cname,clean_session=True):
    #flags set
    client= mqtt.Client(cname)
    if mqttclient_log: #enable mqqt client logging
        client.on_log=on_log
    client.on_connect= on_connect        #attach function to callback
    client.on_message=on_message        #attach function to callback
    client.on_subscribe=on_subscribe

    return client
def on_message(client, userdata, message):
    #time.sleep(1)
    topic=message.topic
    msg=str(message.payload.decode("utf-8"))
    if message.retain==1:
        print("message received  ",msg,"topic",topic,"retained Flag=True",)
        retained_topics.append((topic,msg))#store 
    
def on_connect(client, userdata, flags, rc):
    if rc==0:
        client.connected_flag=True
    else:
        client.bad_connection_flag=True
        if rc==5:
            print("broker requires authentication")
def on_log(client, userdata, level, buf):
        print("log: "+buf)
def on_subscribe(client, userdata, mid, granted_qos):
    #print("subscribed ok ")
    client.suback_flag=True            
def on_publish(client, userdata, mid):
    print("message published "  )

def clear_retained(retained): #accepts single topic or list
    msg=""
    for t in retained:
        print ("Clearing retained on ",msg,"topic -",t[0]," qos=",QOS0," retain=",RETAIN)
        client.publish(t[0],msg,qos=QOS0,retain=RETAIN)
        time.sleep(1)

    
##############



print("Creating client  with clean session set to ",CLEAN_SESSION)
Initialise_client_object()#create object flags
client= Initialise_clients(cname)


print("connecting to broker ",broker,"on port ",port,\
      " topic",topic)
try:
    res=client.connect(broker,port)           #establish connection
except:
    print("can't connect to broker",broker)
    sys.exit()

client.loop_start()

while not client.connected_flag and not client.bad_connection_flag:
    time.sleep(.25)
if client.bad_connection_flag:
    print("connection failure to broker ",broker)
    client.loop_stop()
    sys.exit()
    
client.subscribe(topic)
sleep_count=0
while not client.suback_flag: #wait for subscribe to be acknowledged
    time.sleep(.25)
    if sleep_count>40: #give up
        print("Subscribe failure quitting")
        client.loop_stop()
        sys.exit()
    sleep_count+=1
delay=10   
print("checking wait for ",delay," seconds")
time.sleep(delay)#wait for messages that indicate retianed message
if len(retained_topics)>0:
    print("Found these topics with possible retained messages")
    for t in retained_topics:
        print("topic =",t[0],"  Message= ",t[1])
else:
    print("No topics with retained messages found")
print("unsubscribing")
client.unsubscribe(topic)
print("enable logging")
client.on_log=on_log  
print("publishing to topic house/sensor1 wth retained flag set")
client.publish("house/sensor1","retained test message",qos=0, retain=True)
print(" subscribing to ",topic)
client.subscribe(topic)
time.sleep(5)
#now we clear all retained messages before ending script
print("clearing retained messages ")
if len(retained_topics)>0:
    verbose=False
    clear_retained(retained_topics)
time.sleep(2)
client.loop_stop()
client.disconnect()
print("disconnecting")
print("Ending")


