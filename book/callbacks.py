#! c:\python34\python.exe
#!/usr/bin/env python
##demo code provided by Steve Cope at www.steves-internet-guide.com
##email steve@steves-internet-guide.com
##Free to use for any purpose
"""
Demonstrates callbacks- If you set the QOS to ) you will notice that the
on_publish callback isn't triggered. It also demonstrates getting
messages into main script from the on_message callback using a queue
Question Why isn't the on_subscribe callback triggered.
Answer at bottom
"""
import paho.mqtt.client as mqtt  #import the client
import time
from queue import Queue
q=Queue()
messages=[]
broker="test.mosquitto.org"
broker="192.168.1.41"
#broker="localhost"
QOS=0
QOS=1 #Set to 1 or 2 and you see on publish callback
def on_connect(client, userdata, flags, rc):
    client.connected_flag=True
    #messages.append(m)
    #print(m)

def on_message(client, userdata, message):
    #global messages
    m="message received  "  ,str(message.payload.decode("utf-8"))
    q.put(m) #put messages on queue
    print("message received  ",m)
def on_publish (client, userdata, mid):
    print("on publish callback mid "+str(mid))

def on_subscribe(client, userdata, mid, granted_QOS):
    print("on_subscribe callback mid "+str(mid))
  

client = mqtt.Client("P1")    #create new instance
client.on_connect= on_connect        #attach function to callback
client.on_message=on_message        #attach function to callback
client.on_publish =on_publish        #attach function to callback
#client.on_subscribe =on_subscribe        #attach function to callback
time.sleep(1)
print("connecting to broker")
client.connected_flag=False
client.connect(broker)      #connect to broker
print("starting the loop")
client.loop_start()    #start the loop
print("subscribing QOS=",QOS)
r=client.subscribe("house/bulbs/#",QOS)
while not client.connected_flag:
    print("waiting for connect")
    time.sleep(0.5)
for i in range(3):
    print("publishing")
    m="test message number =" +str(i)
    client.publish("house/bulbs/bulb1",m,QOS)
    time.sleep(1)

while not q.empty():
    message = q.get()
    print("queue: ",message)
client.disconnect()
client.loop_stop()
"""
on_subscribe callback isn't triggered because it is missing the
assignement.client.on_Subscribe =on_Subscribe
"""

