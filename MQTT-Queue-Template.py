#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
A template to be used as starting point for new MQTT projects.

Copyright 2022 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme

Published under EUPL.

Acknowledgements
----------------
- The MQTT part is inpired by http://www.steves-internet-guide.com
"""

import datetime
import logging
import os
import platform
from platformdirs import PlatformDirs
import queue
import re
import sys
import threading
import time
import paho.mqtt.client as mqtt

logger = logging.getLogger(re.split('.py', os.path.basename(__file__))[0])

broker = 'localhost'
hostname = platform.node()
client_name = hostname + '_' + re.split('.py', os.path.basename(__file__))[0]
date_time = re.split(' ', str(datetime.datetime.now()))
client_id = client_name  # + '_' + date_time[0] + '_' + date_time[1]
topic_command = client_id + '/Command'
topic_status = client_id + '/Status'

q = queue.Queue()


def on_log(client, userdata, level, buf):
    """Log buffer if callback is assigned."""
    logger.debug(f'log: {buf}')
    # print('log: ' + buf)
    pass


def on_connect(client, userdata, flags, rc):
    """
    Handle broker connected callback.

    Publish all topic once.
    Subscribe to desired topics.
    """
    if rc == 0:
        client.connected_flag = True
        logger.info('Connected to broker.')
        publish_all(client)
        subscribe_all(client)
    else:
        logger.error(f'Bad connection, returned code = {rc}')
    pass


def on_disconnect(client, userdata, flags, rc=0):
    """Handle broker disconnected callback."""
    # client.connected_flag = False
    logger.debug(f'Disconnected from broker. Result code = {(rc)}')
    pass


def on_publish(client, userdata, mid):
    """Handle publish callback."""
    logger.debug(f'Client published message ID = {mid}')
    pass


def on_message(client, userdata, msg):
    """
    Handle message received callback.

    Decode received message data and insert into command processor queue.
    """
    topic = msg.topic
    m_decode = str(msg.payload.decode('utf-8', 'ignore'))
    logger.debug(f'Message received. Topic: {topic} Payload: {m_decode}')
    q.put((topic, m_decode))
    pass


def on_subscribe(client, userdata, mid, granted_qos):
    """Handle subscribed callback."""
    logger.debug(f'Client subscribed message ID = {mid} with qos = {granted_qos}')
    pass


def on_unsubscribe(client, userdata, mid):
    """Handle unsubscribed callback."""
    logger.debug(f'Client unsubscribed message ID = {mid}')
    pass


def publish_all(client):
    """Publish all topics."""
    rc, mid = client.publish(client_id, 'connected')
    logger.debug(f'Publishing: "connected" returned rc = {rc} mid = {mid}')
    rc, mid = client.publish(topic_status, 0)
    logger.debug(f'Publishing: {topic_status} returned rc = {rc} mid = {mid}')
    rc, mid = client.publish(topic_command, 'Write your command here.')
    logger.debug(f'Publishing: {topic_command} returned rc = {rc} mid = {mid}')
    pass


def subscribe_all(client):
    """Subscribe to all topics."""
    rc, mid = client.subscribe(topic_command)
    rc, mid = client.subscribe(topic_status)
    logger.debug(f'Subscribing to: {topic_command} returned rc = {rc} mid = {mid}')
    pass


def unsubscribe_all(client):
    """Unsubscribe from all topics."""
    rc, mid = client.unsubscribe(topic_command)
    logger.debug(f'Unsubscribing from: {topic_command} returned rc = {rc} mid = {mid}')
    pass


def command_processor():
    """
    Implement command processing here.

    Get decoded message from queue and process.
    Example: Handle topic 'Command'.
    """
    while True:
        received = q.get()
        topics = re.split('/', received[0])
        data = received[1]
        logger.debug(f'Command processing: {topics[1]}')
        match topics[1]:
            case 'Command':
                logger.info(f'{topics[1]}: {data}')
            case 'Status':
                logger.debug(f'{topics[1]}: {data}')
            case _:
                logger.warning(f'Received unkown: {received[0]}')
        logger.debug('Command processing done.')
        q.task_done()
        pass


def periodic_processor(client, ii):
    """Perform periodic actions."""
    client.publish(topic_status, ii)
    pass


if __name__ == '__main__':
    app_name = re.split('.py', os.path.basename(__file__))[0]
    print(app_name, '\n\
    Copyright 2021 GSI Helmholtzzentrum für Schwerionenforschung GmbH\n\
    Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany\n\
    eMail: H.Brand@gsi.de\n\
    Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme\n\
    Published under EUPL.')
    # Configure logging
    match os.name:
        case 'nt':
            user_name = os.getlogin()
            pass
        case 'posix':
            import pwd
            user_name = pwd.getpwuid(os.geteuid()).pw_name
            pass
        case _:
            user_name = os.getlogin()
            pass
    # print(f'OS User: {user_name}')
    dirs = PlatformDirs(app_name, user_name)
    log_filename = os.path.join(dirs.user_log_dir, app_name + '.log')
    if not os.path.exists(os.path.dirname(log_filename)):
        os.makedirs(os.path.dirname(log_filename), exist_ok=True)
    print('Log filename:', log_filename)
    # logging.basicConfig(filename=app_name + '.log', filemode='w', encoding='utf-8', level=logging.INFO, style='{', format='{asctime} [{levelname:8}] {process} {thread} {module} {funcName} {lineno} {message}')
    logger = logging.getLogger(app_name)
    logging_formatter = logging.Formatter(style='{', fmt='{asctime} [{levelname:8}] {module} {funcName} {lineno} {message}')
    console_log_handler = logging.StreamHandler()
    console_log_handler.setFormatter(logging_formatter)
    file_log_handler = logging.FileHandler(log_filename, encoding='utf-8', mode='w')  # mode='a' or 'w'
    file_log_handler.setFormatter(logging_formatter)
    # Add logging handlers to logger
    logger.addHandler(console_log_handler)
    logger.addHandler(file_log_handler)
    # Set debug levels
    logger.setLevel(logging.INFO)
    console_log_handler.setLevel(logging.INFO)
    file_log_handler.setLevel(logging.DEBUG)
    # Insert you main code here
    try:
        # client = mqtt.Client(client_id='', clean_session=True, userdata=None, protocol=MQTTv311, transport='tcp')
        client = mqtt.Client(client_id, clean_session=True)
        client.connected_flag = False
        client.will_set(client_id, 'Offline', 1, False)
        # bind call back function
        client.on_connect = on_connect
        client.on_disconnect = on_disconnect
        # client.on_log=on_log
        client.on_publish = on_publish
        client.on_message = on_message
        client.on_subscribe = on_subscribe
        client.on_unsubscribe = on_unsubscribe
        logger.info(f'Connecting client {client_id} to broker {broker}')
        # turn-on the worker thread
        threading.Thread(target=command_processor, daemon=True).start()
        client.loop_start()
        client.connect(broker, port=1883, keepalive=60, bind_address='')
        while not client.connected_flag:
            print('Waiting for', broker, '...')
            time.sleep(1)
        time.sleep(3)
        print('Waiting for exception (Ctrl+c) ...')
        ii = 0
        while client.connected_flag:
            ii += 1
            periodic_processor(client, ii)
            time.sleep(1)
    except KeyboardInterrupt:
        logger.debug('Srcipt stopped by user! (Ctrl+c)')
    except BaseException:
        logger.exception('Exception catched!')
        client.connected_flag = False
    finally:
        unsubscribe_all(client)
        q.join()
        logger.debug('Publishing: disconnected')
        rc, mid = client.publish(client_id, 'disconnected')
        logger.debug(f'Publishing: disconnected returned rc = {rc} mid = {mid}')
        logger.debug(f'Disonnecting from broker: {broker}')
        client.disconnect()
        time.sleep(1)
        logger.debug('Stopping message loop')
        client.loop_stop(force=False)
        print('Script execution stopped.')
        sys.exit()
