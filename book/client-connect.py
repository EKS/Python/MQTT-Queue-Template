#! c:\python34\python.exe
#!/usr/bin/env python
##demo code provided by Steve Cope at www.steves-internet-guide.com
##email steve@steves-internet-guide.com
##Free to use for any purpose
"""
Try connecting to a non existant broker and see what you get
Rhe script also demonstrates generating client names using two methods
a random number and time.Notice I use a prefix
"""
broker="test.mosquitto.org"
broker="192.168.1.41"
#broker="localhost"
import paho.mqtt.client as mqtt  #import the client1
import time
import random
def on_log(client, userdata, level, buf):
        print("log: "+buf)
def on_connect(client, userdata, flags, rc):
    if rc==0:
        print("connected OK")
    else:
        print("Bad connection Returned code=",rc)
#edit accorfingly
cname="testclient-"+str(int(time.time()))
#r=random.randrange(1,10000)
#cname="testclient" +"-"+str(r)
client = mqtt.Client(cname)#create new instance

client.on_connect=on_connect  #bind call back function
client.on_log=on_log

print("Connecting to broker ",broker)

client.connect(broker)      #connect to broker
client.loop_start()  #Start loop 

time.sleep(4)
client.loop_stop()    #Stop loop 
client.disconnect() # disconnect



