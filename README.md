MQTT-Queue-Template
===================
- MQTT-Queue-Template.py is a template to be used as starting point for new MQTT projects.
- MQTT-Recorder contains an example utilizing the MQTT-Recorder to log selected messages and plot historical data. 

Copyright 2022 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme

Published under EUPL.

Dependencies
------------
Refer to import statements.

Known issues
------------
- Supported data types, all converted to float:
  - boolean {false/true}
  - integer and 
  - float, only.

Copyright 2022 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme

Published under EUPL V. 1.1. Refer to license files on disc.

Acknowledgements
----------------
- The MQTT part is inpired by http://www.steves-internet-guide.com

Revision History
----------------
Revision 1.0.0.0 - 13.05.2022 H.Brand@gsi.de
- Initial commit.